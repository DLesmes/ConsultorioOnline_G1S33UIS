# [ConsultorioOnline](https://gitlab.com/DLesmes/ConsultorioOnline_G1S33UIS/-/blob/main/README.md) 
#### Grupo 1 del curso Desarrollo de Software (S33) de la [Universidad Industrial de Santander](https://lms.uis.edu.co/) (UIS) Dentro del marco del programa [MisiónTic2022](https://www.misiontic2022.gov.co/portal/)

Este repositorio es el código fuente del modelo mínimo viable inicial de una **aplicación web** en **Java** para el backend y **Angular** (HTML, CSS, Javascript) para el Frontend, desplegado a través de un servidor web local [Tomcat](http://tomcat.apache.org/)

El proyecto se distribuye en dos carpetas:
## [Webapp](https://gitlab.com/DLesmes/ConsultorioOnline_G1S33UIS/-/tree/main/ConsultorioApp/src/main/webapp)
Donde se aloja todo lo relacionado a Angular y el frontend

* **login/registro** [source](https://gitlab.com/DLesmes/ConsultorioOnline_G1S33UIS/-/blob/main/ConsultorioApp/src/main/webapp/index.jsp)

![image](https://user-images.githubusercontent.com/61529697/137435829-e5742c1e-3120-4a8e-805f-13546661baf5.png)

![image](https://user-images.githubusercontent.com/61529697/137436267-d98e9456-7ae3-4411-bca8-68f03ca4b8e7.png)

* **Barra de navegación** [source](https://gitlab.com/DLesmes/ConsultorioOnline_G1S33UIS/-/blob/main/ConsultorioApp/src/main/webapp/navBar.jsp)

![image](https://user-images.githubusercontent.com/61529697/137435882-da56033c-b552-455d-8014-75e04f252e1f.png)

* **Perfil** [source](https://gitlab.com/DLesmes/ConsultorioOnline_G1S33UIS/-/blob/main/ConsultorioApp/src/main/webapp/perfil.jsp)

![image](https://user-images.githubusercontent.com/61529697/137435892-a719d047-d8f7-4cf3-99d8-b75b516dfa25.png)

* **Listar** [source](https://gitlab.com/DLesmes/ConsultorioOnline_G1S33UIS/-/blob/main/ConsultorioApp/src/main/webapp/listener.jsp)

![image](https://user-images.githubusercontent.com/61529697/137435909-73a86f3d-e070-4b4f-ad07-b507baf7a4d3.png)

* **Contactanos** [source](https://gitlab.com/DLesmes/ConsultorioOnline_G1S33UIS/-/blob/main/ConsultorioApp/src/main/webapp/contact_us.jsp)

![image](https://user-images.githubusercontent.com/61529697/137435922-01d8f160-4cb8-46e1-922e-e4cad6bbe47c.png)

* **Peticiones** [source](https://gitlab.com/DLesmes/ConsultorioOnline_G1S33UIS/-/blob/main/ConsultorioApp/src/main/webapp/peticiones.jsp)

## [Java](https://gitlab.com/DLesmes/ConsultorioOnline_G1S33UIS/-/tree/main/ConsultorioApp/src/main/java)
Donde se aloja todo lo relacionado a Java y el backend
* **Logica** [source](https://gitlab.com/DLesmes/ConsultorioOnline_G1S33UIS/-/tree/main/ConsultorioApp/src/main/java/logica)
* **ConexionBD** [source](https://gitlab.com/DLesmes/ConsultorioOnline_G1S33UIS/-/tree/main/ConsultorioApp/src/main/java/persistencia)

## Base Datos
La base de datos de [Mysql](https://www.mysql.com/), se encuentra alojada en un servidor propio de la universidad

## **Software Development Process** [source](https://github.com/DLesmes/MisionTic2022/tree/main/SoftwareEngineering/Project_ConsultorioOnline)
Acá se relacionan todos los Sprints desarrollados a lo largo del ciclo 3 del programa

